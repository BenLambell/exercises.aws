This repository is part of an exercise I'm doing to learn AWS.

#Notes#

Will probably need personal account.
If using Xero, note we can set up access keys (for the CLI) by finding self on list of users and going to one of the tabs.

#The plan#

To set up a static site in S3 which calls a web service in EC2.
Files: https://bitbucket.org/BenLambell/exercises.aws

1. Create an EC2 instance via the AWS console, and give it a role with access to AWS
1. SSH into the EC2 instance
1. Copy the static site onto the EC2 instance (https://unix.stackexchange.com/questions/106480)
1. Create an S3 bucket via the AWS CLI
1. Upload the static site to S3
1. Verify the site is accessible to the public
1. Copy the Web API service onto the EC2 instance (we'll need a Windows Server instance)
1. Host the Web API service via iisexpress (https://forums.asp.net/t/2071405.aspx)
1. Verify the Web API service accessible to the public
1. Modify the static site to call the Web API service

Later...

- Create a DynamoDB instance, to persist data
- Create a Lambda function (for the sake of it), to replace the hashing function in the Web API service