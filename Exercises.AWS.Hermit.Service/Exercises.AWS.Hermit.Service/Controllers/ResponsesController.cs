﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Exercises.AWS.Hermit.Service.Controllers
{
    public class ResponsesController : ApiController
    {
        private static readonly List<string> Gifts = new List<string>();

        public string GetResponse(string id)
        {
            Gifts.Add(id);
            if (Gifts.Count <= 10)
                return "The hermit accepts " + id + ".";

            var worstGift = Gifts.OrderBy(Hash).FirstOrDefault();
            Gifts.Remove(worstGift);

            if (Equals(worstGift, id))
                return "You offer the hermit " + id + ", but they politely refuse.";

            return "You give the hermit " + id + ". They give you " + worstGift + ".";
        }

        private static int Hash(string s)
        {
            var hash = 0;
            foreach (var c in s)
            {
                hash += Hash(hash + c);
            }
            return hash;
        }

        private static int Hash(int value, uint seed = 0)
        {
            uint c1 = 0xcc9e2d51;
            uint c2 = 0x1b873593;
            int r1 = 15;
            int r2 = 13;
            uint m = 5;
            uint n = 0xe6546b64;

            var result = (uint)value;

            result *= c1;
            result = (result << r1) | (result >> (32 - r1));
            result *= c2;
            result ^= seed;
            result = (result << r2) | (result >> (32 - r2));
            result *= m;
            result += n;

            result ^= 32;
            result ^= result >> 16;
            result ^= 0x85ebca6b;
            result ^= result >> 13;
            result ^= 0xc2b2ae35;
            result ^= result >> 16;

            return (int) result;
        }
    }
}
